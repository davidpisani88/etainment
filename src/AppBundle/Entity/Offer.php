<?php

// src/AppBundle/Entity/Offer.php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;


/**
 *
 * @ORM\Entity
 * @ORM\Table(name="offer")
 */
class Offer
{
    /**
     * @ORM\Id
     * @ORM\Column(name="application_id")
     */
    private $applicationId;

    /**
     * @ORM\Column(name="advertiser_id", type="string")
     */
    private $advertiserId;

    /**
     * @ORM\Column(name="country", type="string")
     */
    private $country;

    /**
     * @ORM\Column(name="payout", type="float")
     */
    private $payout;

    /**
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @ORM\Column(name="platform", type="string")
     */
    private $platform;

    /**
     * @ORM\ManyToOne(targetEntity="Advertiser")
     */
    private $advertiser;


    public function setApplicationId ($applicationId) {
        $this->applicationId = $applicationId;
        return $this;
    }

    public function setCountry ($country) {
        $this->country = $country;
        return $this;
    }

    public function setPrice ($payout) {
        $this->payout = $payout;
        return $this;
    }

    public function setName ($name) {
        $this->name = $name;
        return $this;
    }

    public function setPlatform ($platform) {
        $this->platform = $platform;
        return $this;
    }

    public function setAdvertiserId ($advertiserId) {
        $this->advertiserId = $advertiserId;
        return $this;
    }

    /**
     * Get applicationId.
     *
     * @return string
     */
    public function getApplicationId()
    {
        return $this->applicationId;
    }

    /**
     * Get advertiserId.
     *
     * @return string
     */
    public function getAdvertiserId()
    {
        return $this->advertiserId;
    }

    /**
     * Get country.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set payout.
     *
     * @param int $payout
     *
     * @return Offer
     */
    public function setPayout($payout)
    {
        $this->payout = $payout;

        return $this;
    }

    /**
     * Get payout.
     *
     * @return int
     */
    public function getPayout()
    {
        return $this->payout;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get platform.
     *
     * @return string
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    public function getAdvertiser()
    {
        return $this->advertiser;
    }

    public function setAdvertiser(Advertiser $advertiser)
    {
        $this->advertiser = $advertiser;
    }
}
