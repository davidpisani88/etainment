<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{

    public static function request($params)
    {
        $client = new \GuzzleHttp\Client();

        try {
            $res = $client->request($params['method'], $params['url']);
            $json = json_decode($res->getBody(), true);
            return $json;
        } catch (\GuzzleHttp\Exception\ClientException $e) {

            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();

            return $responseBodyAsString;
        }

    }
}
