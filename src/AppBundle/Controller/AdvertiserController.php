<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Advertiser;

class AdvertiserController extends Controller
{

    protected $em;

    public function __construct($em)
    {
        $this->em = $em;
    }

    public function addAdvertiser($params) {

        try {
            $user = new Advertiser;
            $user->setName($params['name']);

            $this->em->persist($user);
            $this->em->flush();

            return $user;
        } catch (\Exception $e) {
            return $e;
        }

    }

}
