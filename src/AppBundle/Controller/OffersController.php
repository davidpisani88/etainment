<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Offer;
use AppBundle\Entity\Advertiser;
use AppBundle\Controller\ApiController as Api;
use AppBundle\Controller\AdvertiserController;

class OffersController extends Controller
{

    private $advertiser;

    public function initial($adId) {

        $em = $this->getDoctrine()->getManager();

        //check if advertiser is in the database
        $advertiser = $this->getDoctrine()->getRepository('AppBundle\Entity\Advertiser')->findOneByName($adId);
        
        if(!$advertiser){
            $params['name'] = $adId;

            $adc = new AdvertiserController($em);
            $advertiser = $adc->addAdvertiser($params);
        }

        $this->advertiser = $advertiser;

        //get offer data
        $this->getOfferInfo($this->advertiser->getId());

        $offers = $this->getDoctrine()->getRepository('AppBundle\Entity\Offer')->findBy(
            array('advertiser' => $this->advertiser)
        );

        if($offers){
            $this->prettyResult($offers);
        }

    }

    public function getOfferInfo($adId) {

        $params['url'] = 'http://process.xflirt.com/advertiser/'.$adId.'/offers';
        $params['method'] = 'GET';

        // Call API to retrieve data
        $request = Api::request($params);

        // if result is an array, this is a success
        if(is_array($request)){

            foreach($request as $req){

                if($adId == 2){

                    $check = $this->getDoctrine()->getRepository('AppBundle\Entity\Offer')->find($req['campaigns']['cid']);

                    $params['appId'] = $req['campaigns']['cid'];
                    $params['country'] = substr($req['campaigns']['countries'][0], 0, 2);
                    $params['price'] = $req['campaigns']['points'];
                    $params['name'] = $req['app_details']['bundle_id'];
                    $params['platform'] = $req['app_details']['platform'];

                }else{
                    $check = $this->getDoctrine()->getRepository('AppBundle\Entity\Offer')->find($req['campaign_id']);

                    $params['appId'] = $req['campaign_id'];
                    $params['country'] = substr($req['countries'][0], 0, 2);
                    $params['price'] = $req['payout_amount'];
                    $params['name'] = $req['name'];
                    $params['platform'] = $req['mobile_platform'];
                }

                if(!$check){
                    // if advertiser = 2 convert points to price
                    if($adId == 2){
                        $params['price'] = $this->pointsToCurrency($params['price']);
                    }

                    // add offer to database
                    $this->addOffer($params);
                    $params = [];
                }
            }

            return true;

        }else{
            return false;
        }
    }

    public function pointsToCurrency($points) {

        return  ($points / 10) * 0.01;
    }

    public function addOffer($params) {

        try {
            $offer = new Offer;
            $offer->setApplicationId($params['appId']);
            $offer->setCountry($params['country']);
            $offer->setPrice($params['price']);
            $offer->setName($params['name']);
            $offer->setPlatform($params['platform']);
            $offer->setAdvertiser($this->advertiser);

            $em = $this->getDoctrine()->getManager();
            $em->persist($offer);
            $em->flush();

            return true;
        } catch (\Exception $e) {
            return $e;
        }
    }


    public function prettyResult ($result) {

        echo "====== Offers for advertiser ".$this->advertiser->getName()." ====== \n";

        foreach($result as $res){
            echo 'ID: ' . $res->getApplicationId();
            echo ', Country: ' . $res->getCountry();
            echo ', Payout: ' . $res->getPayout() .' USD';
            echo ', Name: ' . $res->getName();
            echo ', Platform: ' . $res->getPlatform();
            echo "\n";
        }

        return true;
    }
}
