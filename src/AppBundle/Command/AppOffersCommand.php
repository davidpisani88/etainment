<?php

namespace AppBundle\Command;

use AppBundle\Controller\OffersController;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Controller\OffersController as Offers;

class AppOffersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:offers-command')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::REQUIRED, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        //argument is the advertiser id
        $argument = $input->getArgument('argument');

        $offerContainer = $this->getContainer()->get('app.offer');

        $offer = $offerContainer->initial($argument);

        if($offer){
            return true;
        }else{
            return false;
        }
    }

}
